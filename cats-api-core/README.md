# cats-api-core

Для запуска необходимо передать следующие переменные окружения
* NODE_PORT - порт приложения
* POSTGRES_USER - пользователь pg
* POSTGRES_PASSWORD - пароль пользователя pg
* POSTGRES_DB - БД pg
* POSTGRES_PORT - порт pg
* POSTGRES_HOST - хост pg
* SERVICE_VERSION - версия сервиса
* SWAGGER_BASE_PATH - начальный путь до swagger