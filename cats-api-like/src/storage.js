const {Pool} = require('pg');
const {pgUser, pgPass, pgDb, pgHost, pgPort} = require('./configs.js');

const pool = new Pool({
  user: pgUser,
  database: pgDb,
  password: pgPass,
  host: pgHost,
  port: pgPort,
  connectionTimeoutMillis: 5000,
});

pool.on('error', (err) => {
  console.error('Error database', err);
  process.exit(-1);
});


/**
 * Добавление лайка коту
 * @param catId
 * @returns {*|query|void|Promise<PermissionStatus>}
 */
async function plusLike(catId) {
  const updateResult = await pool.query('UPDATE Cats SET likes = likes + 1 WHERE id = $1 RETURNING *', [catId]);
  if (updateResult.rows.length === 0) {
    throw {code: 404, message: `Кот с id=${catId} не найден`};
  }
  return updateResult.rows[0];
}


/**
 * Удаление лайка коту
 * @param catId
 * @returns {*|query|void|Promise<PermissionStatus>}
 */
async function minusLike(catId) {
  const selectResult = await pool.query('SELECT * FROM cats WHERE id = $1', [catId]);
  if (selectResult.rows.length === 0) {
    throw {code: 404, message: `Кот с id=${catId} не найден`};
  } else if (selectResult.rows[0].likes === 0) {
    return selectResult.rows[0];
  }
  const updateResult=await pool.query('UPDATE Cats SET likes = likes - 1 WHERE id = $1 RETURNING *', [catId]);
  return updateResult.rows[0];
};

/**
 * Добавление дизлайка коту
 * @param catId
 * @returns {*|query|void|Promise<PermissionStatus>}
 */
async function plusDislike(catId) {
  const updateResult = await pool.query('UPDATE Cats SET dislikes = dislikes + 1 WHERE id = $1  RETURNING *', [catId]);
  if (updateResult.rows.length === 0) {
    throw {code: 404, message: `Кот с id=${catId} не найден`};
  }
  return updateResult.rows[0];
}

/**
 * Удаление дизлайка коту
 * @param catId
 * @returns {*|query|void|Promise<PermissionStatus>}
 */
async function minusDislike(catId) {
  const selectResult = await pool.query('SELECT * FROM cats WHERE id = $1', [catId]);
  if (selectResult.rows.length === 0) {
    throw {code: 404, message: `Кот с id=${catId} не найден`};
  } else if (selectResult.rows[0].dislikes === 0) {
    return selectResult.rows[0];
  }
  const updateResult = await pool.query('UPDATE Cats SET dislikes = dislikes - 1 WHERE id = $1 RETURNING *', [catId]);
  return updateResult.rows[0];
}

/**
 * Список котов с наибольшим количеством лайков
 * @param limit
 * @returns {*|query|void|Promise<PermissionStatus>}
 */
async function getLikesRating(limit = 10) {
  limit = Number(limit) || 10;

  return (await pool.query(`SELECT id, name, likes FROM cats ORDER BY likes DESC, LOWER(name) LIMIT ${limit}`)).rows;
}

/**
 * Список котов с наибольшим количеством дизлайков
 * @param limit
 * @returns {*|query|void|Promise<PermissionStatus>}
 */
async function getDislikesRating(limit = 10) {
  limit = Number(limit) || 10;

  return (await pool.query(`SELECT id, name, dislikes FROM cats ORDER BY dislikes DESC, LOWER(name) LIMIT ${limit}`)).rows;
}

module.exports = {
  pool,
  plusLike,
  minusLike,
  plusDislike,
  minusDislike,
  getLikesRating,
  getDislikesRating,
};
